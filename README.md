# DSO Platform - Frontend
 
![REPO-TYPE](https://img.shields.io/badge/repo--type-frontend-success?style=for-the-badge&logo=react)
 
The DSO Platform is a software component developed within the ENERSHARE project.
Access to the DSO Platform is via a graphical interface, represented by a GUI, through which users can:
 
- View, for specific ASM sensors, the graphical representation of the historical values of the corresponding metrics of interest.
 
- Furthermore, it gives access to an auction mechanism, where the DSO (Distribution System Operator), in order to manage energy flexibility, requests "x kw/h" to be consumed, for a possible energy surplus, opening an auction, characterized for a specific duration.
 
The DSO Platform also includes interfaces that help the DSO in determining the energy and duration values, to be entered via the interface during the opening of the auction.
 
## Built With
 
- [React](https://react.dev/) - UI Library
 
## Configuring
 
Here is the list of the environment variables configurable in the .env file (for the standard installation) and in the Dockerfile (for the docker installation) in the root of the project.
 
| VARIABLE NAME                  | DESCRIPTION                                                       | DEFAULT VALUE                                |
| ------------------------------ | ----------------------------------------------------------------- | -------------------------------------------- |
| **NODE_BACKEND_BASE_URL** | The base url of the dso platform backend instance | `http://localhost:3000/` |
| **REACT_API_URL** | The url of the backend API | `http://localhost:8080/api/dataenergyvalue` |

&nbsp;
 
## Getting Started
 
### Prerequisites
 
* React 17+
 
 
### Installing
 
- Check out the code from this repository:
 
```
git clone https://gitlab.com/dt-iot/enershare/dso-platform-frontend.git
```
 
- Move into the root directory of the application:
 
```
cd dso-platform-frontend
```
 
- Run the following command:
 
```
npm install
```
 
### Running
 
In the project directory, run the following command:
 
```
npm start
```
 
&nbsp;
 
## Getting Started Using Docker
 
### Prerequisites
 
- [Docker](https://docs.docker.com/get-started/)
 
 
### Installing
 
Check out the code from this repository:
 
```
git clone https://gitlab.com/dt-iot/enershare/dso-platform-frontend.git
```
 
Move into the root directory of the application:
 
```
cd dso-platform-frontend
```
 
### Running
 
Run the following command:
 
```
docker run -d $(docker build -q .)
```