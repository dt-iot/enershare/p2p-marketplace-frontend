# Use the latest LTS version of Node.js
FROM node:18-alpine

# Create the working directory inside the container
RUN mkdir /app

# Set the working directory inside the container
WORKDIR /app

# Copy package.json and package-lock.json
COPY package*.json ./

# Install dependencies
RUN npm install

# Copy the rest of your application files
COPY . .

ENV REACT_API_URL=http://localhost:8080/api/dataenergyvalue

# Expose the port your app runs on
EXPOSE 3000

# Define the command to run your app
CMD ["npm", "start"]

